interface Menu {
    name: string 
    subMenu: SubMenu[]
  }
   
  interface SubMenu {
    name: string
  }
   
  const menus: Menu[] = [
    {
      name: 'Home',
      subMenu: [],
    },
    {
      name: 'About',
      subMenu: [
        {
          name: 'Company',
        },
        {
          name: 'Team',
        },
      ],
    },
    {
      name: 'Products',
      subMenu: [
        {
          name: 'Electronics',
        },
        {
          name: 'Clothing',
        },
        {
          name: 'Accessories',
        },
      ],
    },
    {
      name: 'Services',
      subMenu: [],
    },
    {
      name: 'Contact',
      subMenu: [
        {
          name: 'Phone',
        },
      ],
    },
    {
      name: 'Blog',
      subMenu: [],
    },
    {
      name: 'Gallery',
      subMenu: [
        {
          name: 'Photos',
        },
        {
          name: 'Videos',
        },
        {
          name: 'Events',
        },
      ],
    },
    {
      name: 'FAQ',
      subMenu: [],
    },
    {
      name: 'Downloads',
      subMenu: [
        {
          name: 'Documents',
        },
        {
          name: 'Software',
        },
      ],
    },
    {
      name: 'Support',
      subMenu: [
        {
          name: 'Help Center',
        },
        {
          name: 'Contact Us',
        },
        {
          name: 'Knowledge Base',
        },
      ],
    },
  ];

const ex1 = document.getElementById("ex1") as HTMLDivElement

for (const menu of menus){
    const ul = document.createElement("ul")
    const li = document.createElement("li")
    const name = document.createTextNode(menu.name)
    li.appendChild(name)
    ul.appendChild(li)
    for (const submenu of menu.subMenu){
        if (menu.subMenu.length > 0){
            const subUl = document.createElement("ul")
            const subLi = document.createElement("li")
            const subName = document.createTextNode(submenu.name)
            subLi.appendChild(subName)
            subUl.appendChild(subLi)
            ul.appendChild(subUl)
        }
    }
    
    ex1.appendChild(ul)
    
}


export {}